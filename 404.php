<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package influencer-internship
 */

get_header();
?>

<?php 
	$blog_background_image	= get_theme_mod( 'blog_background_setting', esc_url( get_template_directory_uri() . '/images/header-bg.jpg' ) );
	?>
		<div class="page-header" style="background: url(<?php echo $blog_background_image; ?>) no-repeat;">
			<div id="primary" class="content-area">
				<main id="main" class="site-main">
					<section class="error-404 not-found">
					</section>
				</main>
			</div>
		</div>
		<div class="page-content">
			<div class="cm-wrapper">
				<h1 class="entry-title"><?php _e( 'Page Not Found', 'influencer-internship' ); ?></h1>
				<a class="bttn" href="<?php echo esc_url( home_url('/') ); ?>"><?php esc_html_e( 'Take me to the home page','influencer-pro' );?></a>
				<br><b> <?php esc_html_e( 'OR', 'influencer-internship' )?></b></br>
				<?php get_search_form(); ?>
			</div>
		</div>
<?php
get_footer();
