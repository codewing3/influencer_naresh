<?php get_header(); ?>
<div id="content" class="site-content">
        <div class="page-header" style="background: url(images/header-bg.jpg) no-repeat;">
            <div class="cm-wrapper">
                <h1 class="page-title">About Us</h1>
                <div class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                    <div id="crumbs">
                        <a href="#" class="home_crumb">Home</a> 
                        <span class="separator">&#9866;</span> 
                        <span class="current">About Us</span>
                    </div>
                </div>
            </div>
        </div>
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                <div class="cm-wrapper">
                    <div class="about-story">
                        <div class="story-content-wrap">
                            <div class="entry-header">
                                <h4 class="entry-subtitle">Things Behind Our Success</h4>
                                <h2 class="entry-title">Our Story</h2>
                            </div>
                            <div class="story-desc">
                                <p><span class="text-blue font-700">Sifting through teaspoons of clay and sand scraped from the floors of caves,</span> German researchers have managed to isolate ancient human DNA — without turning up a single bone.</p>

                                <p>It’s a bit like discovering that you can extract gold dust from the air,” said Adam Siepel, a population geneticist at Cold Spring Harbor Laboratory.An absolutely amazing and exciting paper,” added David Reich, a genetics professor. Harvard who focuses on ancient DNA. Until recently, the only way to study the genes of ancient humans like the Neanderthals and their cousins, the Denisovans, was to recover DNA from fossil bones.</p>

                                <p>But they are scarce and hard to find, which has greatly limited research into where early humans lived and how widely they ranged. The only Denisovan bones and teeth that scientists have, for example, come from a single cave in Siberia. Looking for these genetic signposts in sediment has become possible only in the last few years, with recent developments in technology.</p>
                            </div>
                        </div>
                        <figure class="story-img">
                            <img src="images/story-img.jpg" alt="story image">
                        </figure>
                    </div>
                </div>
                <div class="story-feat-wrap clearfix">
                    <section class="widget widget_media_image">
                        <a href="#">
                            <img src="images/story-feat-img.jpg" class="image" alt="">
                        </a>
                    </section>
                    <div class="widget-wrap">
                        <section class="widget widget_rrtc_icon_text_widget">        
                            <div class="rtc-itw-holder">
                                <div class="rtc-itw-inner-holder">
                                    <div class="text-holder">
                                        <h2 class="widget-title">On Time Service</h2>
                                        <div class="content">
                                            <p>It’s a bit like discovering that you can extract gold dust from the air,” said Adam Siepel, a population geneticist at Cold Spring Harbor Laboratory.</p>
                                        </div>                            
                                    </div>
                                    <div class="icon-holder">
                                        <span class="fa fa-clock-o"></span>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section class="widget widget_rrtc_icon_text_widget">        
                            <div class="rtc-itw-holder">
                                <div class="rtc-itw-inner-holder">
                                    <div class="text-holder">
                                        <h2 class="widget-title">A Team of Professionals</h2>
                                        <div class="content">
                                            <p>It’s a bit like discovering that you can extract gold dust from the air,” said Adam Siepel, a population geneticist at Cold Spring Harbor Laboratory.</p>
                                        </div>                             
                                    </div>
                                    <div class="icon-holder">
                                        <span class="fa fa-users"></span>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section class="widget widget_rrtc_icon_text_widget">        
                            <div class="rtc-itw-holder">
                                <div class="rtc-itw-inner-holder">
                                    <div class="text-holder">
                                        <h2 class="widget-title">Analyze Your Business</h2>
                                        <div class="content">
                                            <p>It’s a bit like discovering that you can extract gold dust from the air,” said Adam Siepel, a population geneticist at Cold Spring Harbor Laboratory.</p>
                                        </div>                             
                                    </div>
                                    <div class="icon-holder">
                                        <span class="fa fa-line-chart"></span>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <section class="story-statcounter-section" style="background: url(images/stat-counter-bg.jpg) no-repeat;">
                    <div class="cm-wrapper">
                        <div class="section-widget-wrap">
                            <section class="widget widget_text">
                                <h2 class="widget-title">Success Stats</h2>
                                <div class="textwidget">
                                    <p>things behind our success</p>
                                </div>
                            </section>
                            <section class="widget widget_raratheme_companion_stat_counter_widget">
                                <div class="col">
                                    <div class="raratheme-sc-holder">
                                        <h2 class="widget-title">All time clients</h2>
                                        <div class="icon-holder">
                                            <i class="fa fa-address-book"></i>
                                        </div>
                                        <div class="hs-counter">
                                            <div class="odometer odometer-auto-theme" date-count="50000">
                                                <div class="odometer-inside">
                                                    10,000
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="widget widget_raratheme_companion_stat_counter_widget">
                                <div class="col">
                                    <div class="raratheme-sc-holder">
                                        <h2 class="widget-title">Ongoing Projects</h2>
                                        <div class="icon-holder">
                                            <i class="fa fa-address-book"></i>
                                        </div>
                                        <div class="hs-counter">
                                            <div class="odometer odometer-auto-theme" date-count="50000">
                                                <div class="odometer-inside">
                                                    250
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="widget widget_raratheme_companion_stat_counter_widget">
                                <div class="col">
                                    <div class="raratheme-sc-holder">
                                        <h2 class="widget-title">Success Rate</h2>
                                        <div class="icon-holder">
                                            <i class="fa fa-address-book"></i>
                                        </div>
                                        <div class="hs-counter">
                                            <div class="odometer odometer-auto-theme" date-count="50000">
                                                <div class="odometer-inside">
                                                    98
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="widget widget_raratheme_companion_stat_counter_widget">
                                <div class="col">
                                    <div class="raratheme-sc-holder">
                                        <h2 class="widget-title">projects Completed</h2>
                                        <div class="icon-holder">
                                            <i class="fa fa-address-book"></i>
                                        </div>
                                        <div class="hs-counter">
                                            <div class="odometer odometer-auto-theme" date-count="50000">
                                                <div class="odometer-inside">
                                                    1845
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
                <section class="story-team-section">
                    <div class="cm-wrapper">
                        <div class="section-widget-wrap">
                            <section class="widget widget_text">
                                <h2 class="widget-title">Success Stats</h2>
                                <div class="textwidget">
                                    <p>things behind our success</p>
                                </div>
                            </section>
                            <section class="widget widget_rrtc_description_widget">
                                <div class="rtc-team-holder">
                                    <div class="rtc-team-inner-holder">
                                        <div class="image-holder">
                                            <img src="images/team-img1.jpg" alt="">
                                        </div>

                                        <div class="text-holder">
                                            <span class="name">Woodrow Nikolaus</span>
                                            <span class="designation">Digital Marketing Manager</span>
                                            <div class="description">The comms landscape is more complex than ever. Brands are required to create an ever increasing volume of hard working content and none of the traditi</div>                              
                                        </div>
                                        <ul class="social-profile">
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-linkedin"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-instagram"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-youtube"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-dribbble"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-behance"></i>
                                                </a>
                                            </li>                    
                                        </ul>
                                    </div>
                                </div>

                                <div class="rtc-team-holder-model">
                                    <div class="rtc-team-inner-holder-model">
                                        <div class="image-holder">
                                            <img src="images/team-img1.jpg" alt="">
                                        </div>

                                        <div class="text-holder">
                                            <span class="name">Woodrow Nikolaus</span>
                                            <span class="designation">Digital Marketing Manager</span>
                                            <div class="description">The comms landscape is more complex than ever. Brands are required to create an ever increasing volume of hard working content and none of the traditi</div>                              
                                        </div>
                                        <ul class="social-profile">
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-linkedin"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-instagram"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-youtube"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-dribbble"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-behance"></i>
                                                </a>
                                            </li>                    
                                        </ul>
                                    </div>
                                    <a href="#" class="close_popup">closepopup</a>
                                </div>
                            </section>
                            <section class="widget widget_rrtc_description_widget">
                                <div class="rtc-team-holder">
                                    <div class="rtc-team-inner-holder">
                                        <div class="image-holder">
                                            <img src="images/team-img2.jpg" alt="">
                                        </div>

                                        <div class="text-holder">
                                            <span class="name">Woodrow Nikolaus</span>
                                            <span class="designation">Digital Marketing Manager</span>
                                            <div class="description">The comms landscape is more complex than ever. Brands are required to create an ever increasing volume of hard working content and none of the traditi</div>                              
                                        </div>
                                        <ul class="social-profile">
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-linkedin"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-instagram"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-youtube"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-dribbble"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-behance"></i>
                                                </a>
                                            </li>                    
                                        </ul>
                                    </div>
                                </div>

                                <div class="rtc-team-holder-model">
                                    <div class="rtc-team-inner-holder-model">
                                        <div class="image-holder">
                                            <img src="images/team-img2.jpg" alt="">
                                        </div>

                                        <div class="text-holder">
                                            <span class="name">Woodrow Nikolaus</span>
                                            <span class="designation">Digital Marketing Manager</span>
                                            <div class="description">The comms landscape is more complex than ever. Brands are required to create an ever increasing volume of hard working content and none of the traditi</div>                              
                                        </div>
                                        <ul class="social-profile">
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-linkedin"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-instagram"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-youtube"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-dribbble"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-behance"></i>
                                                </a>
                                            </li>                    
                                        </ul>
                                    </div>
                                    <a href="#" class="close_popup">closepopup</a>
                                </div>
                            </section>
                            <section class="widget widget_rrtc_description_widget">
                                <div class="rtc-team-holder">
                                    <div class="rtc-team-inner-holder">
                                        <div class="image-holder">
                                            <img src="images/team-img3.jpg" alt="">
                                        </div>

                                        <div class="text-holder">
                                            <span class="name">Woodrow Nikolaus</span>
                                            <span class="designation">Digital Marketing Manager</span>
                                            <div class="description">The comms landscape is more complex than ever. Brands are required to create an ever increasing volume of hard working content and none of the traditi</div>                              
                                        </div>
                                        <ul class="social-profile">
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-linkedin"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-instagram"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-youtube"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-dribbble"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-behance"></i>
                                                </a>
                                            </li>                    
                                        </ul>
                                    </div>
                                </div>

                                <div class="rtc-team-holder-model">
                                    <div class="rtc-team-inner-holder-model">
                                        <div class="image-holder">
                                            <img src="images/team-img3.jpg" alt="">
                                        </div>

                                        <div class="text-holder">
                                            <span class="name">Woodrow Nikolaus</span>
                                            <span class="designation">Digital Marketing Manager</span>
                                            <div class="description">The comms landscape is more complex than ever. Brands are required to create an ever increasing volume of hard working content and none of the traditi</div>                              
                                        </div>
                                        <ul class="social-profile">
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-linkedin"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-instagram"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-youtube"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-dribbble"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-behance"></i>
                                                </a>
                                            </li>                    
                                        </ul>
                                    </div>
                                    <a href="#" class="close_popup">closepopup</a>
                                </div>
                            </section>
                            <section class="widget widget_rrtc_description_widget">
                                <div class="rtc-team-holder">
                                    <div class="rtc-team-inner-holder">
                                        <div class="image-holder">
                                            <img src="images/team-img4.jpg" alt="">
                                        </div>

                                        <div class="text-holder">
                                            <span class="name">Woodrow Nikolaus</span>
                                            <span class="designation">Digital Marketing Manager</span>
                                            <div class="description">The comms landscape is more complex than ever. Brands are required to create an ever increasing volume of hard working content and none of the traditi</div>                              
                                        </div>
                                        <ul class="social-profile">
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-linkedin"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-instagram"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-youtube"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-dribbble"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-behance"></i>
                                                </a>
                                            </li>                    
                                        </ul>
                                    </div>
                                </div>

                                <div class="rtc-team-holder-model">
                                    <div class="rtc-team-inner-holder-model">
                                        <div class="image-holder">
                                            <img src="images/team-img4.jpg" alt="">
                                        </div>

                                        <div class="text-holder">
                                            <span class="name">Woodrow Nikolaus</span>
                                            <span class="designation">Digital Marketing Manager</span>
                                            <div class="description">The comms landscape is more complex than ever. Brands are required to create an ever increasing volume of hard working content and none of the traditi</div>                              
                                        </div>
                                        <ul class="social-profile">
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-linkedin"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-instagram"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-youtube"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-dribbble"></i>
                                                </a>
                                            </li>                        
                                            <li>
                                                <a target="_blank" href="#">
                                                    <i class="fa fa-behance"></i>
                                                </a>
                                            </li>                    
                                        </ul>
                                    </div>
                                    <a href="#" class="close_popup">closepopup</a>
                                </div>
                            </section>
                        </div>
                    </section>
                </main>
            </div>
        </div>
    </div>

<?php get_footer(); ?>