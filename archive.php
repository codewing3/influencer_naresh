<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package influencer-internship
 */

get_header();
?>
	<div id="content" class="site-content">
	<?php 
	$blog_background_image	= get_theme_mod( 'blog_background_setting', esc_url( get_template_directory_uri() . '/images/header-bg.jpg' ) );
	?>
		<div class="page-header" style="background: url(<?php echo $blog_background_image; ?>) no-repeat;">
			<div class="cm-wrapper">
				<?php the_archive_title( '<h1 class="page-title">', '</h1>' ); 
				the_archive_description( '<div class="archive-description">', '</div>' );?>
					<a href="#primary" class="scroll-down"></a>
			</div>
		</div>
		<div class="cm-wrapper">
			<div id="primary" class="content-area">
				<main id="main" class="site-main">
					<?php if ( have_posts() ) : 
						while ( have_posts() ) :
							the_post();
							get_template_part( 'template-parts/content', get_post_type() );
						endwhile;
						influencer_internship_pagination();
					else :
						get_template_part( 'template-parts/content', 'none' );
					endif;
					?>
				</main>
			</div>
			<?php get_sidebar(); ?>
		</div>

<?php

get_footer();
