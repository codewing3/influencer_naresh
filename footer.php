<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package influencer-internship
 */

?>

<footer class="site-footer" itemscope itemtype="http://schema.org/WPFooter">
	<?php influencer_internship_top_footer(); ?>
	<?php influencer_internship_bottom_footer(); ?>
</footer><!-- .site-footer -->


<?php wp_footer(); ?>

</body>
</html>
