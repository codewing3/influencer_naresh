<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package influencer-internship
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<header class="site-header" itemscope itemtype="http://schema.org/WPHeader">
		<?php influencer_internship_header_ticker();?>
		<div class="main-header">
<<<<<<< HEAD
			<div class="cm-wrapper">
				<?php if( has_custom_logo() || display_header_text() ) : 
                    if( has_custom_logo() && display_header_text() ) {
                        $add_class = 'has-text-img';
                    }else{
                        $add_class = '';
                    } ?>
                    
                    <div class="site-branding <?php echo esc_attr( $add_class ); ?>" itemscope itemtype="https://schema.org/Organization">
                        <?php if( has_custom_logo() ) : ?>
                            <div class="site-logo">
                                <?php the_custom_logo(); ?>
                            </div>
                        <?php endif; ?>
                        <?php if( display_header_text() ) { ?>
                            <div class="site-title-wrap">
                                <?php if ( is_front_page() ) : ?>
										<h1 class="site-title" itemprop="name"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
									<?php else : ?>
										<p class="site-title" itemprop="name"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
								<?php endif;
                                $description = get_bloginfo( 'description', 'display' );
								if ( $description || is_customize_preview() ) : ?>
									<p class="site-description" itemprop="description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
									<?php
                                endif; ?>
                            </div>
                        <?php } ?>
                    </div><!-- .site-branding -->
				<?php endif; ?>
				<div class="nav-wrap">
					<nav class="main-navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary',
							'fallback_cb'    => 'influencer_internship_add_menu',
						)
					);
					?>
				</div>	
			</div>
=======
			<?php influencer_internship_header_navigation(); ?>
>>>>>>> 3cf3030202226f5feb5e04bccb8e3e6844348cc7
		</div><!-- .main-header -->
	</header><!-- .site-header -->