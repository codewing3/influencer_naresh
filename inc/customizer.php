<?php
/**
 * influencer-internship Theme Customizer
 *
 * @package influencer-internship
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function influencer_internship_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'influencer_internship_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'influencer_internship_customize_partial_blogdescription',
			)
		);
		}
	}
add_action( 'customize_register', 'influencer_internship_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function influencer_internship_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function influencer_internship_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function influencer_internship_customize_preview_js() {
	wp_enqueue_script( 'influencer-internship-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), _S_VERSION, true );
}
add_action( 'customize_preview_init', 'influencer_internship_customize_preview_js' );

if ( ! function_exists( 'influencer_sanitize_checkbox' ) ) {
    /**
     * Switch sanitization
     *
     * @param string $input Switch value.
     * @return integer  Sanitized value
     */
    function influencer_sanitize_checkbox( $input ) {
        if ( false === $input ) {
            return 0;
        } else {
            return 1;
        }
    }
}

function influencer_internship_sanitize_select( $input, $setting ) {

	// Ensure input is a slug.
	$input = sanitize_key( $input );
  
	// Get list of choices from the control associated with the setting.
	$choices = $setting->manager->get_control( $setting->id )->choices;
  
	// If the input is a valid key, return it; otherwise, return the default.
	return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
}

function influencer_sanitize_get_authors($select = true, $slug = false){
	$user = array();
	$userlists = get_users();
	if( $select ) $user [''] = __( 'Choose User', 'influencer-internship' );
	foreach ($userlists as $user){
		if( $slug ){
			$users[$user->slug]		=	$user->name;
		} else {
			$users[$user->term_id]	=	$user->name;
		}
	}
	return $users;
}

/**
 * Sanitize radio.
 *
 * @param $input   
 * @param WP_Customize_Setting $setting Setting instance.
 */
function influencer_internship_sanitize_radio( $input, $setting ) {
    // Ensure input is a slug.
    $input = sanitize_key( $input );
    // Get list of choices from the control associated with the setting.
    $choices = $setting->manager->get_control( $setting->id )->choices;
    // If the input is a valid key, return it; otherwise, return the default.
    return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
}