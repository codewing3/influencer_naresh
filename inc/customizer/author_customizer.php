<?php 
function influencer_internship_about_author_customizer($wp_customize){
    $wp_customize->add_panel('front_page_panel', array(
        'title'       => __( 'Front Page Panel', 'influencer-internship' ),
        'description' => esc_html__( 'Customize author info', 'influencer-internship' ),
        'priority'    => 30,
    ) );
    /**
     * Footer Copyright Area Section.
     */
    $wp_customize->add_section('about_author_section', array(
            'title'       => esc_html__( 'About author', 'influecner-internship' ),
            'description' => __( 'Modify <b>About author</b>.', 'influecner-internship' ),
            'panel'       => 'front_page_panel',
            'priority'    => 40,
    ) );
    // Default copyright.
    $wp_customize->add_setting( 'author_setting_title', array(
        'default'                       => __( 'Sarah Baker', 'influencer-internship' ), 

    ) );
    $wp_customize->add_control( 'author_setting_title', array(
        'label'                       	=>  __( 'author', 'influencer-internship' ),     
        'section'                    	=>  'about_author_section',
        'type'							=> 'select',
        'sanitize_callback'				=> influencer_sanitize_get_authors(),
    ) );
    $wp_customize->add_setting( 'author_image_upload', array(
        'default'                       => get_template_directory_uri() . '/images/about-img.jpg',   
        'sanitize_callback'             => 'esc_url_raw',
        'section'						=> 'about_author_section',
    
    ) );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'author_image_upload',
            array(
                'label'			=> __( 'Upload Author Image', 'influencer-internship' ),
                'section'		=> 'about_author_section',
        
            )
    ) );
}
add_action('customize_register','influencer_internship_about_author_customizer');