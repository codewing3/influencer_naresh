<?php 
function influencer_internship_blog_background_image( $wp_customize ) {
    $wp_customize->add_panel('blog_page', array(
        'title'       => __( 'Blog Panel', 'influencer-internship' ),
        'description' => esc_html__( 'Customize Blogs', 'influencer-internship' ),
        'priority'    => 100,
    ) );
    $wp_customize->add_section( 'blog_section' , array(
        'title' => __( 'Blog Section' ),
        'panel' => 'blog_page',
    ) );
    $wp_customize->add_setting( 'blog_background_setting', array(
        'default'                       => get_template_directory_uri() . '/images/header-bg.jpg',   
        'sanitize_callback'             => 'esc_url_raw',
    
    ) );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'blog_background_setting',
            array(
                'label'			=> __( 'Upload Blog Background Image', 'influencer-internship' ),
                'section'		=> 'blog_section',
                'setting'       => 'blog_background_setting'
        
            )
    ) );

    $wp_customize->add_section( 
        'blog_layout',
         array(
            'capability'  => 'edit_theme_options',
            'title'       => esc_html__( 'Blog Layout', 'influencer-internship' ),
            'panel'       => 'blog_page',
            'priority'    => 30,
        ) 
    );
    /** Blog layout */
    $wp_customize->add_setting( 
        'blog_layout_option', 
        array(
            'default'           => ' ',
            'sanitize_callback' => 'influencer_internship_sanitize_radio'
        ) 
    );
    $wp_customize->add_control(
        'blog_layout_option',
            array(
                'type'		  => 'radio',
                'section'     => 'blog_layout',
                'label'       => esc_html__( 'Choose Blog Layout', 'influencer-internship' ),
                'description' => esc_html__( 'This is the general layout for blog, archive & search.', 'influencer-internship' ),
                'choices'     => array(
                    'default-layout'=> __('Blog Layout 1'),
                    'grid-layout'   => __('Blog Layout 2'),
                )
            ) 
    );

    $wp_customize->add_section( 'default_image_section' , array(
        'title' => __( 'Default Image for Post Section' ),
        'panel' => 'blog_page',
    ) );
    $wp_customize->add_setting( 'default_image_setting', array(
        'default'                       => get_template_directory_uri() . '/images/header-bg.jpg',   
        'sanitize_callback'             => 'esc_url_raw',
    
    ) );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'default_image_setting',
            array(
                'label'			=> __( 'Upload an default Image for posts background', 'influencer-internship' ),
                'section'		=> 'default_image_section',
                'setting'       => 'default_image_setting',
        
            )
    ) );

}
add_action('customize_register', 'influencer_internship_blog_background_image');