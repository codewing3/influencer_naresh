<?php
function influencer_internship_client_customizer($wp_customize){

		$wp_customize->add_section('client_logo_section', array(
			'title'       => esc_html__( 'Client Logo Section', 'influecner-internship' ),
			'description' => __( 'Modify <b>Logos of our Clients</b>.', 'influecner-internship' ),
			'panel'       => 'front_page_panel',
			'priority'    => 15,
		) );
		$wp_customize->add_setting('client_logo_number_setting', array(
			'default'                       => '3',   
			'sanitize_callback'             => 'absint',
			'section'						=> 'client_logo_section',
			'transport'         			=> 'refresh',

		) );
		$wp_customize->add_control('client_logo_number_setting', array(
			'type' 				=> 'select',
			'section' 			=> 'client_logo_section',
			'label'				=> __( 'Select logos number' ),
			'description' 		=> __( 'This is a custom select option.' ),
			'choices'	 		=> array(
			  '1'					=> __( '1' ),
			  '2' 					=> __( '2' ),
			  '3' 					=> __( '3' ),
			  '4'					=> __( '4' ),
			  '5' 					=> __( '5' ),
			  '6' 					=> __( '6' ),
			),
		  ) );
		
		$logo_value		=	get_theme_mod('client_logo_number_setting');
		for( $i = 1; $i <= $logo_value; $i++ ){
			$wp_customize->add_setting('client_logo_upload_setting_' . $i, array(
				'default'                       => '',   
				'section'						=> 'client_logo_section',
			) );
			$wp_customize->add_control(new WP_Customize_Image_Control(
				$wp_customize,
				'client_logo_upload_setting_' . $i, array(
					'label'		=>	__($i . '--Choose Image--','influencer-internship'),
					'section'	=>	'client_logo_section',
					'settings'	=>	'client_logo_upload_setting_' . $i,
				)
			) );
		}
    }
add_action('customize_register', 'influencer_internship_client_customizer');