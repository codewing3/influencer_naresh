<?php 
function influencer_internship_footer_customizer($wp_customize){
    /**
     * Footer Copyright Area Section.
     */
    $wp_customize->add_section( 'footer_copyright_section', array(
            'title'       => esc_html__( 'Footer Copyright Area', 'influecner-internship' ),
            'description' => __( 'Modify <b>Footer Copyright Area</b>.', 'influecner-internship' ),
            'panel'       => 'front_page_panel',
            'priority'    => 50,
    ) );
    // Default copyright.
    $wp_customize->add_setting(
        'influencer_footer_copyright_checkbox',
        array(
            'default'           => true,
            'transport'         => 'postMessage',
            'sanitize_callback' => 'influencer_sanitize_checkbox',
        )
    );

    $wp_customize->add_control(
        'influencer_footer_copyright_checkbox',
        array(
            'label'       => __( 'Enable/Disable Default Copyright.', 'midday' ),
            'description' => __( 'This checkbox, once <b>unchecked</b>,<br>removes <b>Default Copyright.</b>', 'midday' ),
            'section'     => 'footer_copyright_section',
            'type'        => 'checkbox',
        )
    );

    $wp_customize->selective_refresh->add_partial(
        'influencer_footer_copyright_checkbox',
        array(
            'selector'        => '#site-info',
            'settings'        => array( 'influencer_footer_copyright_checkbox' ),
            'render_callback' => 'midday_site_info',
        )
    );
    // Custom copyright.
    $wp_customize->add_setting( 'influencer_custom_copyright_textarea', array(
            'default'           => __( '© Copyright 2017, Content Marketing. All Rights Reserved. Theme by Rara Theme. Powered by WordPress.' ),
            'transport'         => 'postMessage',
            'sanitize_callback' => 'wp_kses_post', // Allow html.
    ) );
    $wp_customize->add_control( 'influencer_custom_copyright_textarea', array(
            'label'       => esc_html__( 'Custom Copyright Textarea', 'influencer-internship' ),
            'description' => __( 'To display a <b>Custom Copyright</b>,<br><b>uncheck</b> the <b>Default Copyright</b> checkbox,<br>then type a custom copyright in the textarea.<br><b>HTML</b> is allowed !', 'influencer-internship' ),
            'section'     => 'footer_copyright_section',
            'type'        => 'textarea',
            'input_attrs' => array(
                'style'       => 'border: 1px solid #999',
                'placeholder' => __( 'Enter Custom Copyright...', 'influencer-internship' ),
            ),
    ) );
    $wp_customize->selective_refresh->add_partial( 'influencer_custom_copyright_textarea', array(
            'selector'        => '#site-info',
            'settings'        => array( 'influencer_custom_copyright_textarea' ),
            'render_callback' => 'influencer_site_info',
    ) );
}
add_action('customize_register','influencer_internship_footer_customizer');