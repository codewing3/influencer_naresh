<?php 
function influencer_internship_head_customizer($wp_customize){
    $wp_customize->add_section( 'header_section' , array(
        'title'                         => __( 'header section' ),
        'panel'                         => 'front_page_panel',
        'priority'                      => '10',

    ) );
    $wp_customize->add_setting( 'header_setting', array(
        'default'                       => __( 'The ultimate e-mail marketing course is out now!', 'influencer-internship' ),   
        'sanitize_callback'             =>  'esc_html',
        'section'						=> 'header_section',

    ) );
    $wp_customize->add_control( 'header_setting', array(
        'label'                       	=>  __( 'header', 'influencer-internship' ),     
        'section'                    	=>  'header_section',
        'type'							=> 'text',
    ) );

    $wp_customize->add_setting( 'header_button_setting', array(
        'default'						=> '',
        'capability' 					=> 'edit_theme_options',
        'transport' 					=> '',
        'sanitize_callback' 			=> 'text',

    ) );
    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'header_button_setting', array(
            'label'          => __( 'Choose button type', 'influencer-internship' ),
            'section'        => 'header_section',
            'settings'       => 'header_button_setting',
            'type'           => 'radio',
            'choices'        => array(
                'dark'   => __( 'Primary' ),
                'light'  => __( 'Secondary' )
            )
        )
    ) );
    $wp_customize->add_setting( 'header_button_text', array(
        'default'                       => __( 'Register Today!!!', 'influencer-internship' ),   
        'sanitize_callback'             =>  'esc_html',
        'section'						=> 'header_section',

    ) );
    $wp_customize->add_control( 'header_button_text', array(
        'label'                       	=>  __( 'header', 'influencer-internship' ),     
        'section'                    	=>  'header_section',
        'type'							=> 'text',
    ) );
    $wp_customize->add_section( 'banner_section' , array(
        'title' => __( 'Banner Section' ),
        'panel' => 'header_panel',
    ) );
    $wp_customize->add_setting( 'banner_settings', array(
        'default'                       => get_template_directory_uri() . '/images/banner-img1.jpg',   
        'sanitize_callback'             => 'esc_url_raw',
        'section'						=> 'banner_section',
    
    ) );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'banner_settings',
            array(
                'label'			=> __( 'Upload Banner Image', 'influencer-internship' ),
                'section'		=> 'banner_section',
        
            )
    ) );
    $wp_customize->add_setting( 'banner_subscribe_settings', array(
        'default'                       => get_template_directory_uri() . '/images/ban-caption.png',   
        'sanitize_callback'             => 'esc_url_raw',
        'section'						=> 'banner_section',
    
    ) );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'banner_subscribe_settings',
            array(
                'label'			=> __( 'This is default subscribe image', 'influencer-internship' ),
                'section'		=> 'banner_section',
        
            )
    ) );
}
add_action('customize_register', 'influencer_internship_head_customizer');