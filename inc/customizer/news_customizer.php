<?php 
function influencer_internship_news_customizer( $wp_customize ){

    $wp_customize->add_section( 'news_section' , array(
        'title' => __( 'News Section', 'influencer-internship' ),
        'panel' => 'front_page_panel',
    ) );
    $wp_customize->add_setting( 'news_title_setting', array(
        'default'                   => __( 'Latest News', 'influencer-internship' ),   
        'sanitize_callback'         => 'sanitize_text_field',
    ) );
    $wp_customize->add_control( 'news_title_setting', array(
        'label'			=> __( 'This is news Title', 'influencer-internship' ),
        'section'		=> 'news_section',
        'type'          => 'text',
        'setting'       => 'news_title_setting'
    ));
    $wp_customize->add_setting( 'news_subtitle_setting', array(
        'default'                   => __( 'TIPS FOR GETTING THINGS DONE', 'influencer-internship' ),   
        'sanitize_callback'         => 'wp_kses_post',
    ) );
    $wp_customize->add_control( 'news_subtitle_setting', array(
        'label'			=> __( 'This is subtitle for news', 'influencer-internship' ),
        'section'		=> 'news_section',
        'type'          => 'textarea',
        'setting'       => 'news_subtitle_setting'
    ));

}
add_action( 'customize_register', 'influencer_internship_news_customizer' );