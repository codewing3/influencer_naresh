<?php 
function influencer_internship_page_customizer( $wp_customize ){
    $wp_customize->add_panel('page_panel', array(
        'title'       => __( 'Page Panel', 'influencer-internship' ),
        'description' => esc_html__( 'Customize Pages', 'influencer-internship' ),
        'priority'    => 110,
    ) );
    $wp_customize->add_section( 'page_section' , array(
        'title' => __( 'Page Section', 'influencer-internship' ),
        'panel' => 'page_panel',
    ) );
    $wp_customize->add_setting( 'page_background_setting', array(
        'default'                       => get_template_directory_uri() . '/images/header-bg.jpg',   
        'sanitize_callback'             => 'esc_url_raw',
    ) );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'page_background_setting',
            array(
                'label'			=> __( 'Upload Blog Background Image', 'influencer-internship' ),
                'section'		=> 'page_section',
                'setting'       => 'page_background_setting'
        
            )
    ) );
    $wp_customize->add_setting( 'page_title_setting', array(
        'default'                   => __( 'Landing Page', 'influencer-internship' ),   
        'sanitize_callback'         => 'sanitize_text_field',
    ) );
    $wp_customize->add_control( 'page_title_setting', array(
        'label'			=> __( 'This is the landing page', 'influencer-internship' ),
        'section'		=> 'page_section',
        'type'          => 'text',
        'setting'       => 'page_title_setting'
    ));
}
add_action( 'customize_register', 'influencer_internship_page_customizer' );