<?php 
//create metabox
function influencer_internship_meta_boxes(){
    add_meta_box( 
        'meta-postWidth',
        __( 'MetaBox Details','influencer-internship' ),
        'influencer_internship_meta_callback',
        'post',
        'normal' 
    );
}
add_action( 'add_meta_boxes', 'influencer_internship_meta_boxes' );

function influencer_internship_meta_callback( $post ){
    wp_nonce_field( basename( __FILE__ ), 'meta_width_nonce' );
    $metakey = get_post_meta( $post->ID,'_width_meta',true );
    ?>
        <label for="width_meta"><?php _e( "Post Width",'influencer-internship' ); ?></label>
        <br />
		<select name="meta-postWidth" id="meta-postWidth">
			<option value="rightsidebar" <?php selected( $metakey, 'rightsidebar' ); ?>><?php _e( 'Right Sidebar', 'influencer-internship' ); ?></option>
			<option value="single-fullwidth-layout full-width" <?php selected( $metakey, 'single-fullwidth-layout full-width' ); ?>><?php _e( 'Full width', 'influencer-internship' ); ?></option>
			<option value="single-centered-layout full-width" <?php selected( $metakey, 'single-centered-layout full-width' ); ?>><?php _e( 'Centered Full width', 'influencer-internship' ); ?></option>
		</select>  
    <?php 
}
function influencer_internship_save_meta( $post_id ){
    if ( !isset( $_POST['meta_width_nonce'] ) || !wp_verify_nonce( $_POST['meta_width_nonce'], basename( __FILE__ ) ) )
    return;

    // Stop WP from clearing custom fields on autosave
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )  
    return;

	if ( ! current_user_can( 'edit_post', $post_id ) ){
		return;
	}

    $metawp =  isset( $_POST['meta-postWidth'] ) ? esc_html( $_POST['meta-postWidth'] ) : ""; 

    update_post_meta( $post_id,'_width_meta',$metawp );

}
add_action( 'save_post','influencer_internship_save_meta' );

