<?php
$post = get_the_ID();
?>
<div class="news-block" itemscope itemtype="http://schema.org/Article">
    <figure class="news-block-img">
        <a href="<?php the_permalink(); ?>" itemprop="thumbnailUrl">
            <?php if ( has_post_thumbnail( $post ) ) { ?>
                <figure><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'thumb-size' ); ?></a></figure>
            <?php } else { 
                $related_post_fallback_image    =   get_template_directory_uri() . '/images/post_fallback.jpg' ?>
                <figure><a href="<?php the_permalink(); ?>"><img src="<?php echo esc_url( $related_post_fallback_image ); ?>" alt="<?php _e('fallback image', 'influencer-internship'); ?>" itemprop="image"></a></figure>
            <?php } ?>
        </a>
    </figure>
    <div class="news-content-wrap">
        <h3 class="news-block-title" itemprop="headline">
            <a href="<?php the_permalink(); ?>">
                <?php the_title(); ?>
            </a>
        </h3>
        <div class="entry-meta">
            <span class="byline" itemprop="author">
                <?php esc_html_e( 'by', 'influencer-internship' ); ?>
                <a itemprop="name" class="author vcard" href="<?php the_permalink(); ?>">
                    <?php the_author(); ?>
                </a>
            </span>
            <span class="comments"><?php absint( comments_number() ); ?> </span>
        </div>
        <div class="news-block-desc" itemprop="text">
           <?php the_excerpt(); ?>
        </div>
        <a href="<?php the_permalink(); ?>" class="readmore" itemprop="mainEntityOfPage">
            <?php esc_html_e( 'Continue Reading', 'influencer-internship' ); ?> 
            <img src="<?php echo esc_url( get_template_directory_uri() . '/images/readmore-arrow.png' ); ?>" alt="readmore arrow">
        </a>
    </div>
</div>

