<div class="related-post-block">
    <?php if ( has_post_thumbnail() ) { ?>
        <figure><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'thumb-size' ); ?></a></figure>
    <?php } else { 
        $related_post_fallback_image    =   get_template_directory_uri() . '/images/post_fallback.jpg' ?>
        <figure><a href="<?php the_permalink(); ?>"><img src="<?php echo esc_url( $related_post_fallback_image ); ?>" alt="<?php _e('fallback image', 'influencer-internship'); ?>" itemprop="image"></a></figure>
    <?php } ?>
    <div class="related-content-wrap">
        <h2 class="related-block-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <a href="<?php the_permalink(); ?>" class="readmore"><?php esc_html_e( 'Continue Reading', 'influencer-internship'); ?> <img src="<?php echo esc_url(get_template_directory_uri() . '/images/readmore-arrow.png' ); ?>" alt="readmore arrow"></a>
    </div>
</div>


