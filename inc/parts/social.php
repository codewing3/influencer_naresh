<div class="author-social-links">
    <a href="#" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
    <a href="#" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
    <a href="#" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
    <a href="#" target="_blank" class="snapchat"><i class="fa fa-snapchat-ghost"></i></a>
    <a href="#" target="_blank" class="pinterest"><i class="fa fa-pinterest"></i></a>
    <a href="#" target="_blank" class="google-plus"><i class="fa fa-google-plus"></i></a>
</div>