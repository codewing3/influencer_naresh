<section class="cta-section">
    <div class="cm-wrapper">
        <section class="widget widget_raratheme_companion_cta_widget">        
            <div class="raratheme-cta-container left">
                <h2 class="widget-title">Want to Know More About Us?</h2>
                <div class="text-holder">
                    <p>I'd love to share my experience to help you build your business from scratch to reach your goals, which you can't find elsewhere.</p>
                    <div class="button-wrap">
                        <a href="#" class="btn-cta btn-1">Start Here</a>
                    </div>
                </div>
            </div>         
        </section>
    </div>
</section><!-- .cta-section -->