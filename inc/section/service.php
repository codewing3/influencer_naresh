<section class="service-section">
    <div class="cm-wrapper">
        <div class="section-widget-wrap">
            <section class="widget widget_text">
                <h2 class="widget-title"><?php esc_html_e('Service we provide', 'influencer-internship'); ?></h2>
                <div class="textwidget">
                    <p><?php esc_html_e('what we do regularly?', 'influencer-internship'); ?></p>
                </div>
            </section>
            <section class="widget widget_rrtc_icon_text_widget">        
                <div class="rtc-itw-holder">
                    <div class="rtc-itw-inner-holder">
                        <div class="text-holder">
                            <h2 class="widget-title"><?php esc_html_e('Maximize Global Users', 'influencer-internship'); ?></h2>
                            <div class="content">
                                <p><?php esc_html_e('Sifting through teaspoons of clay and sand scraped from the floors of caves, German researchers have managed to isolate ancient human', 'influencer-internship'); ?></p>
                            </div>
                            <a class="btn-readmore" href="#" target="_blank">Read More</a>                              
                        </div>
                        <div class="icon-holder">
                            <span class="fa fa-bar-chart"></span>
                        </div>
                    </div>
                </div>
            </section>
            <section class="widget widget_rrtc_icon_text_widget">        
                <div class="rtc-itw-holder">
                    <div class="rtc-itw-inner-holder">
                        <div class="text-holder">
                            <h2 class="widget-title">Target E-business</h2>
                            <div class="content">
                                <p>Sifting through teaspoons of clay and sand scraped from the floors of caves, German researchers have managed to isolate ancient human</p>
                            </div>
                            <a class="btn-readmore" href="#" target="_blank">Read More</a>                              
                        </div>
                        <div class="icon-holder">
                            <span class="fa fa-rocket"></span>
                        </div>
                    </div>
                </div>
            </section>
            <section class="widget widget_rrtc_icon_text_widget">        
                <div class="rtc-itw-holder">
                    <div class="rtc-itw-inner-holder">
                        <div class="text-holder">
                            <h2 class="widget-title">Maximize Channels</h2>
                            <div class="content">
                                <p>Sifting through teaspoons of clay and sand scraped from the floors of caves, German researchers have managed to isolate ancient human</p>
                            </div>
                            <a class="btn-readmore" href="#" target="_blank">Read More</a>                              
                        </div>
                        <div class="icon-holder">
                            <span class="fa fa-dashboard"></span>
                        </div>
                    </div>
                </div>
            </section>
            <section class="widget widget_rrtc_icon_text_widget">        
                <div class="rtc-itw-holder">
                    <div class="rtc-itw-inner-holder">
                        <div class="text-holder">
                            <h2 class="widget-title">Exploit Sticky Metrics</h2>
                            <div class="content">
                                <p>Sifting through teaspoons of clay and sand scraped from the floors of caves, German researchers have managed to isolate ancient human</p>
                            </div>
                            <a class="btn-readmore" href="#" target="_blank">Read More</a>                              
                        </div>
                        <div class="icon-holder">
                            <span class="fa fa-trophy"></span>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div></section><!-- .service-section -->