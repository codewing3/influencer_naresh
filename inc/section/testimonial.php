<section class="testimonial-section">
    <div class="cm-wrapper">
        <div class="section-widget-wrap">
            <section class="widget widget_text">
                <h2 class="widget-title">Words from our clients</h2>
                <div class="textwidget">
                    <p>what our clients speak about us</p>
                </div>
            </section>
            <section class="widget widget_rrtc_testimonial_widget">        
                <div class="rtc-testimonial-holder">
                    <div class="rtc-testimonial-inner-holder">
                        <div class="img-holder">
                            <img src="<?php echo esc_url( get_template_directory_uri() . '/images/audio-author1.jpg' ); ?>" alt="Diana">
                        </div>

                        <div class="text-holder">
                            <div class="testimonial-meta">
                                <span class="name">Michael Jordan</span>
                                <span class="designation">Chicago Bulls</span>
                            </div>                              
                            <div class="testimonial-content">
                                <p>“I've missed more than 9000 shots in my career. I've lost almost 300 games. 26 times I've been trusted to take the game winning shot and missed. I've failed over and over and over again in my life. And that is why I succeed.”</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="widget widget_rrtc_testimonial_widget">        
                <div class="rtc-testimonial-holder">
                    <div class="rtc-testimonial-inner-holder">
                        <div class="img-holder">
                            <img src="<?php echo esc_url( get_template_directory_uri() . '/images/audio-author4.jpg'); ?>" alt="Diana">
                        </div>

                        <div class="text-holder">
                            <div class="testimonial-meta">
                                <span class="name">Aristotle</span>
                                <span class="designation">Microsoft</span>
                            </div>                              
                            <div class="testimonial-content">
                                <p>“First, have a definite, clear practical ideal; a goal, an objective. Second, have the necessary means to achieve your ends; wisdom, money, materials, and methods. Third, adjust all your means to that end.”</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="widget widget_rrtc_testimonial_widget">        
                <div class="rtc-testimonial-holder">
                    <div class="rtc-testimonial-inner-holder">
                        <div class="img-holder">
                            <img src="<?php echo esc_url( get_template_directory_uri() . '/images/audio-author3.jpg' );?>" alt="Diana">
                        </div>

                        <div class="text-holder">
                            <div class="testimonial-meta">
                                <span class="name">Helen Keller</span>
                                <span class="designation">Uber</span>
                            </div>                              
                            <div class="testimonial-content">
                                <p>“When one door of happiness closes, another opens, but often we look so long at the closed door that we do not see the one that has been opened for us.”</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="widget widget_rrtc_testimonial_widget">        
                <div class="rtc-testimonial-holder">
                    <div class="rtc-testimonial-inner-holder">
                        <div class="img-holder">
                            <img src="<?php echo esc_url( get_template_directory_uri() . '/images/audio-author5.jpg' ); ?>" alt="Diana">
                        </div>

                        <div class="text-holder">
                            <div class="testimonial-meta">
                                <span class="name">Bob Dylan</span>
                                <span class="designation">Apple</span>
                            </div>                              
                            <div class="testimonial-content">
                                <p>“What’s money? A man is a success if he gets up in the morning and goes to bed at night and in between does what he wants to do.” </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>