<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package influencer-internship
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function influencer_internship_body_classes( $classes ) {
	$metakey = get_post_meta( get_the_ID(),'_width_meta',true );

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[]	= 'rightsidebar';
	}

	if ( is_single() && !empty( $metakey ) ){
		$classes[]	= $metakey;
	} elseif ( is_single() && empty( $metakey )) {
		$classes[]	= 'single-centered-layout full-width';
	}

	if ( is_page()){
		$classes[]	= 'full-width';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[]	= 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'influencer_internship_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function influencer_internship_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'influencer_internship_pingback_header' );

if( ! function_exists( 'influencer_internship_header_ticker' ) ) :

	function influencer_internship_header_ticker(){ ?>
		<?php
		$ticker_title_name		= get_theme_mod('header_setting',  __( 'The ultimate e-mail marketing course is out now!', 'influencer-internship' ) );
		$ticker_button			= get_theme_mod('header_button_setting',  __( 'Choose button type', 'influencer-internship' ) );
		$ticker_button_text		= get_theme_mod('header_button_text',  __( 'header', 'influencer-internship' ) );
		?>
		<div class="header-ticker">
			<span class="ticker-close"><i class="fa fa-close"></i></span>
			<div class="ticker-title-wrap">
				<div class="cm-wrapper">
					<p class="ticker-title">
						<?php echo esc_html($ticker_title_name); ?>
						<a href="#"><?php echo esc_html($ticker_button_text); ?></a>
					</p>
				</div>
			</div>
		</div><!-- .header-ticker -->
	<?php } 
endif;

function influencer_internship_header_navigation(){ ?>
	<div class="cm-wrapper">
		<div class="site-branding" itemscope itemtype="https://schema.org/Organization">
			<?php if( has_custom_logo() ) : ?>
				<div class="site-logo">
					<?php the_custom_logo(); ?>
				</div>
			<?php endif; ?>
			<div>
				<?php if ( is_front_page() ) : ?>
						<h1 class="site-title" itemprop="name"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php else : ?>
						<p class="site-title" itemprop="name"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php endif;
				$description = get_bloginfo( 'description', 'display' );
				if ( $description || is_customize_preview() ) : ?>
					<p class="site-description" itemprop="description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
					<?php
				endif; ?>
			</div>
		</div><!-- .site-branding -->
		<?php influencer_internship_navigation_wrap(); ?>
	</div>
<?php }

function influencer_internship_navigation_wrap(){ ?>
	<div class="nav-wrap">
			<nav class="main-navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary',
					'fallback_cb'    => 'influencer_internship_add_menu',
				)
			);
			?>
		</div>	
<?php }

if( ! function_exists( 'influencer_internship_top_footer' ) ) :

	function influencer_internship_top_footer(){ 
		if (is_active_sidebar('footer-1') || (is_active_sidebar('footer-2')) || (is_active_sidebar('footer-3')) || (is_active_sidebar('footer-4'))){
		?>
		<div class="top-footer">
			<div class="cm-wrapper">
				<?php if ( is_active_sidebar( 'footer-1' ) ) {  
					dynamic_sidebar( 'footer-1' );  } ?>
				<?php if ( is_active_sidebar( 'footer-2' ) ) {  
					dynamic_sidebar( 'footer-2' );  } ?>
				<?php if ( is_active_sidebar( 'footer-3' ) ) {  
					dynamic_sidebar( 'footer-3' );  } ?>
				<?php if ( is_active_sidebar( 'footer-4' ) ) {  
					dynamic_sidebar( 'footer-4' );  } ?>
			</div>
		</div>
		<?php } }
endif;

if( ! function_exists( 'influencer_internship_bottom_footer' ) ) :

	function influencer_internship_bottom_footer(){ ?>
		<div class="bottom-footer">
			<div class="cm-wrapper">
				<?php influencer_internship_site_info(); ?>
				<div class="scroll-to-top">
					<span><?php esc_html_e( 'back to top', 'influencer-internship' ); ?>
						<img src="<?php echo esc_url(get_template_directory_uri() . '/images/scroll-top-arrow.png'); ?>" alt="<?php esc_html_e( 'scroll to top', 'influencer-internship' ); ?>">
					</span>
				</div>
			</div>
		</div>
	<?php }
endif;

if( ! function_exists( 'influencer_internship_site_info' ) ) {

	function influencer_internship_site_info() {
		$influecner_footer_copyright_area_checkbox 	= get_theme_mod( 'influencer_footer_copyright_checkbox', true );
		$influencer_custom_copyright_textarea      	= get_theme_mod( 'influencer_custom_copyright_textarea' );
		if ( ! empty( $influecner_footer_copyright_area_checkbox ) ) :
				$html  = esc_html( '© Copyright 2017, Content Marketing. All Rights Reserved. Theme by ' ) . '<a href = "' . esc_url( 'https://rarathemes.com/' ) . '">' . esc_html( 'Rara Theme. ' ) . '</a>' . esc_html( 'Powered by Wordpress.' );
				echo wp_kses(
					$html,
					array(
						'a'  => array(
							'href' => array(),
						),
					) );
		elseif ( empty( $influecner_footer_copyright_area_checkbox ) && ! empty( $influencer_custom_copyright_textarea ) ) :
			echo wp_kses_post( $influencer_custom_copyright_textarea ); // Allow html.
		endif;
	}
}

function influencer_internship_banner_image(){ 
	$banner_image 				= get_theme_mod( 'banner_settings' , get_template_directory_uri() . '/images/banner-img1.jpg' );
	$banner_subscribe_image		= get_theme_mod( 'banner_subscribe_settings' , get_template_directory_uri() . '/images/ban-caption.png' ); 
	?>
		<div class="banner-section">
			<div class="banner-img">
			<div class="ban-img-holder" style="background: url(<?php echo esc_url( get_header_image() );?>) no-repeat;"></div>
				</div>
				<div class="cm-wrapper">
					<img src="<?php echo esc_url($banner_subscribe_image) ?>" alt="<?php esc_html_e( 'banner caption', 'influencer-internship' ); ?>">
				</div>
				<a href="#clients" class="scroll-down"></a>
			</div>
		</div><!-- .banner-section -->
<?php }

function influencer_internship_add_menu(){
	if( current_user_can( 'manage_options' ) ){
		echo '<ul id="menu-1" class="menu">';
		echo '<li><a href="' . esc_url( admin_url( 'nav-menus.php' ) ) . '">' . esc_html__( 'Click here to add a menu', 'influencer-internship' ) . '</a></li>';
		echo '</ul>';
	}
}

function influencer_internship_logo_section(){ 
	$logo_value		=	get_theme_mod( 'client_logo_number_setting' );?>
	<section id="clients" class="client-section">
		<div class="cm-wrapper">
			<section class="widget widget_raratheme_client_logo_widget">            
				<div class="raratheme-client-logo-holder">
					<div class="raratheme-client-logo-inner-holder">
						<?php 
						for( $i = 1; $i <= $logo_value; $i++ ){
							$logo_image	=	get_theme_mod( 'client_logo_upload_setting_' . $i);
							if(! empty( $logo_image )){ ?>
								<div class="image-holder">
									<a href="<?php the_permalink(); ?>" target="_blank">
										<img class="black-white" src="<?php echo esc_url( $logo_image );?>" alt="basekit">
									</a>
								</div>
							<?php } 
						} ?>
					</div>
				</div>
			</section>
		</div>
	</section><!-- .client-section -->
	
		
<?php }

function influencer_internship_about_author(){
	$author_image 		= get_theme_mod( 'author_image_upload', get_template_directory_uri() . '/images/about-img.jpg' );
	?>
<<<<<<< HEAD
	<section class="about-section">
		<div class="cm-wrapper">
			<section class="widget widget_raratheme_featured_page_widget">                
				<div class="widget-featured-holder left">
					<div class="text-holder"> 
						<p class="section-subtitle"><?php esc_html_e( 'ABOUT THE AUTHOR', 'influencer-internship' ); ?></p>
						<h2 class="widget-title"><?php 
						$current_user = wp_get_current_user();
						printf( __( 'I\'m %s', 'influencer-internship' ), esc_html( $current_user->user_firstname ) ) . ' ' .'<br />';
						printf( __( '%s', 'influencer-internship' ), esc_html( $current_user->user_lastname ) ) . '<br />';
						?></h2>
						<div class="featured_page_content">
							<p><span class="dropcap-letter"><?php esc_html_e( 'Hi!', 'influencer-internship' ); ?></span><?php printf( __( '%s', 'influencer-internship' ), esc_html( $current_user->user_description ) ) . '<br />'; ?></p>
							<a href="<?php the_permalink(); ?>" target="_blank" class="btn-readmore"><?php esc_html_e( 'Read More', 'influencer-internship' ); ?></a>
						</div>
					</div>
					<div class="img-holder">
						<a target="_blank" href="#">
							<img src="<?php echo esc_url( $author_image ); ?>" class="wp-post-image" alt="">
						</a>
					</div>
				</div>        
			</section>
		</div>
	</section><!-- .about-section -->
=======
		<section class="about-section">
			<div class="cm-wrapper">
				<?php if ( is_user_logged_in() ) { ?> 
					<section class="widget widget_raratheme_featured_page_widget">                
						<div class="widget-featured-holder left">
							<div class="text-holder"> 
								<p class="section-subtitle"><?php esc_html_e( 'ABOUT THE AUTHOR', 'influencer-internship' ); ?></p>
								<h2 class="widget-title">
									<?php 
									$current_user = wp_get_current_user();
									if ( empty ( $current_user->user_firstname && $current_user->user_lastname ) ){ 
										printf( __( 'I\'m %s ', 'influencer-internship' ), esc_html( $current_user->user_login ) ) . '<br />';							
									} else {
										printf( __( 'I\'m %s ', 'influencer-internship' ), esc_html( $current_user->user_firstname ) )  .'<br />';
										printf( __( '%s', 'influencer-internship' ), esc_html( $current_user->user_lastname ) ) . '<br />';
									} 
									?>
								</h2>
								<div class="featured_page_content">
									<p><span class="dropcap-letter"><?php esc_html_e( 'Hi!', 'influencer-internship' ); ?></span><?php printf( __( '%s', 'influencer-internship' ), esc_html( $current_user->user_description ) ) . '<br />'; ?></p>
									<a href="<?php the_permalink(); ?>" target="_blank" class="btn-readmore"><?php esc_html_e( 'Read More', 'influencer-internship' ); ?></a>
								</div>
							</div>
							<div class="img-holder">
								<a target="_blank" href="#">
									<img src="<?php echo esc_url( $author_image ); ?>" class="wp-post-image" alt="">
								</a>
							</div>
						</div>        
					</section>
				<?php } else {
					echo 'User not logged in.';
				} ?>
			</div>
		</section><!-- .about-section -->
>>>>>>> 3cf3030202226f5feb5e04bccb8e3e6844348cc7
<?php }

function influencer_internship_before_header_index(){ 
	$blog_background_image	= get_theme_mod( 'blog_background_setting', esc_url( get_template_directory_uri() . '/images/header-bg.jpg' ) );
	?>
		<div class="page-header" style="background: url(<?php echo $blog_background_image; ?>) no-repeat;">
			<div class="cm-wrapper">
				<h1 class="page-title"><?php esc_html_e( 'Content Marketing Articles', 'influencer-internship' ); ?></h1>
				<a href="#primary" class="scroll-down"></a>
			</div>
		</div>
<?php }

function influencer_internship_pagination(){
	the_posts_pagination( array(
		'mid_size' => 2,
		'prev_text' => __( '<span class="prev-arrow"></span> <span class="pagination-txt">Previous</span>', 'influencer_internship' ),
		'next_text' => __( '<span class="pagination-txt">Next</span> <span class="next-arrow"></span>', 'influencer_internship' ),
	) );
}

function influencer_internship_grid_content(){ ?>
	<a href="<?php the_permalink(); ?>" class="entry-image" itemprop="thumbnailUrl">
	<?php 
		if ( has_post_thumbnail() ) {
<<<<<<< HEAD
			the_post_thumbnail( 'full' );
=======
			the_post_thumbnail( 'featured-image-size' );
>>>>>>> 3cf3030202226f5feb5e04bccb8e3e6844348cc7
		} else {
			echo '<img src="' . get_template_directory() . '/images/audio-author2.jpg" />';
		}
	?>
	</a>
	<div class="entry-content">
		<header class="entry-header">
			<h2 class="entry-title" itemprop="headline"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<div class="entry-meta">
				<span class="byline" itemprop="author">
					<?php esc_html_e('by','influencer-internship'); ?> <span class="author vcard">
						<a href="<?php the_permalink(); ?>" class="url" itemprop="name"><?php the_author(); ?></a>
					</span>
				</span>
				<span class="comment-box">
					<span class="comment-count"><?php echo absint( get_comments_number() ); ?></span><?php esc_html_e( ' Comments','influencer-internship' ) ?>
				</span>
			</div>
		</header>
		<?php  the_excerpt(); ?>
		<a href="<?php the_permalink(); ?>" class="readmore" itemprop="MainEntityOfPage"><?php esc_html_e( 'Continue Reading','influencer-internship' ); ?></a>
	</div>
<?php }

function influencer_internship_default_style_content(){ ?>
	<header class="entry-header">
		<h2 class="entry-title" itemprop="headline">
			<a href="<?php the_permalink(); ?>"><?php  the_title(); ?></a></h2>
		<div class="entry-meta">
			<span class="posted-on" itemprop="datePublished dateModified">
				<time class="updated published">
					<?php influencer_internship_get_post_time(); ?>	
				</time>
			</span>
			<span class="byline" itemprop="author">
				<?php _e( 'by', 'influencer-internship' ); ?> <span class="author vcard">
					<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" class="url" itemprop="name"><?php the_author(); ?></a>
				</span>
			</span>
			<span class="comment-box">
				<span class="comment-count"><?php echo absint( get_comments_number() ); ?></span> <?php esc_html_e( ' Comments','influencer-internship' ) ?>
			</span>
		</div>
	</header>
	<div class="entry-content">
		<a href="<?php the_permalink(); ?>" >
			<?php
				if ( has_post_thumbnail() ) {
					the_post_thumbnail( 'featured-image-size' );
				} else {
					echo '<img src="' . get_template_directory() . '/images/audio-author2.jpg" />';
				}
			?> 
		</a>
		<?php the_excerpt(); ?>
		<a href="<?php the_permalink(); ?>" class="readmore" itemprop="MainEntityOfPage"><?php esc_html_e( 'Continue Reading', 'influencer-internship' ); ?> <img src="<?php echo esc_url( get_template_directory_uri() . '/images/readmore-arrow.png' ); ?>" alt="readmore arrow"></a>
	</div>
<?php }

function influencer_internship_single_header(){ 
	global $post;
	$author_id	=	$post->post_author;
	$default_post_image = get_theme_mod('default_image_setting', get_template_directory_uri() . '/images/header-bg.jpg');
	$post_image = get_the_post_thumbnail_url( $post->ID );
	if( !empty ($post_image) ){ ?>
	<div class="page-header" style="background: url(<?php echo esc_url( $post_image ); ?>) no-repeat;">
	<?php 
	} else { ?>
	<div class="page-header" style="background: url(<?php echo esc_url( $default_post_image ); ?>) no-repeat;">
	<?php } ?>
		<div class="cm-wrapper">
            <h1 class="page-title" itemprop="headline"><?php  the_title(); ?></h1>
            <div class="entry-meta">
                <span class="posted-on" itemprop="datePublished dateModified">
					<time class="updated published">					
						<?php influencer_internship_get_post_time(); ?>	
					</time>
                </span>
                <span class="byline" itemprop="author">
                    <span class="author vcard">
						<?php _e( 'by ', 'influencer-internship'); ?>
                        <a href="<?php echo esc_url( get_author_posts_url( $author_id ) ); ?>" class="url" itemprop="name"><?php the_author_meta( 'display_name', $author_id ); ?></a>
                    </span>
                </span>
                <span class="comment-box">
                    <span class="comment-count"><?php echo absint( get_comments_number() ); ?></span> <?php esc_html_e( 'Comments', 'influencer-internship' ); ?>
                </span>
            </div>
            <a href="#primary" class="scroll-down"></a>
        </div>
    </div>
<?php }

function influencer_internship_author_newsletter_wrap(){ ?>
	<div class="author-newsletter-wrap">
		<div class="about-author">
			<figure class="author-image">
				<img src="<?php echo get_avatar_url( get_the_author_meta( 'ID' )); ?>" alt="author image"></figure>
			<h3 class="author-name"><?php esc_html_e( 'About the Author', 'influencer-internship' ); ?> <span><?php esc_html( the_author() ); ?></span></h3>
			<div class="author-desc">
				<?php the_author_meta( 'description' ); ?>
			</div>
			<?php get_template_part('inc/parts/social'); ?>
		</div>
		<div class="post-newsletter">
			<img src="<?php echo esc_url( get_template_directory_uri() . '/images/form.jpg' ); ?>" alt="form">
		</div>
	</div>
<?php }

function influencer_internship_comments_part(){
	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;
}
function influencer_internship_comments_field( $fields ){
	$comment_author = __( 'Name*', 'influencer-internship' );
	$comment_email = __( 'E-Mail*', 'infleuncer-internship' );
	$comment_url = __( 'Website', 'influencer-internship' );
	

	$fields['author'] 			  = '<p class="comment-form-author"><br /><label for="author">Name<span class="required">*</span></label><input type="text" id="author" name="author" required="required" placeholder="' . $comment_author .'"></input></p>';
	$fields['email']  			  =	'<p class="comment-form-email"><br /><label for="email">Email<span class="required">*</span></label><input  type="email" id="email" name="email" required="required" placeholder="' . $comment_email .'"></input></p>';
	$fields['url']    			  =	'<p class="comment-form-url"><br /><label for="url">Website</label><input type="url" id="url" name="url" placeholder="' . $comment_url .'"></input></p>';
	unset( $fields['cookies'] );
	return $fields;
}
add_filter( 'comment_form_default_fields', 'influencer_internship_comments_field' );


function influencer_internship_post_navigation_part(){
	the_post_navigation(
		array(
			'prev_text' => '<span class="prev"><span class="prev-arrow"></span><span class="pagination-txt">Previous Posts</span></span><h4>%title</h4>',
			'next_text' => '<span class="next"><span class="next-arrow"></span> <span class="pagination-txt">Next Posts</span></span><h4>%title</h4>'
		) );
}
function influencer_internship_get_post_time(){
	_e( 'Last Updated on ', 'influencer-internship' );
	?><a href="<?php echo get_day_link(get_post_time('Y'), get_post_time('m'), get_post_time('j')); ?>" class="entry-date"><?php the_time('F j, Y') ?></a><?php
}
function influencer_internship_related_post_categories(){ 
	global $post;
	$metakey = get_post_meta( get_the_ID(),'_width_meta',true );
	if ( $metakey === 'rightsidebar'){ 
		$posts_number = '4'; 
	} else { 
		$posts_number = '6'; } 
	$categories = get_the_category();
	$args = array(
		'posts_per_page'    =>  $posts_number,
		'post__not_in'      =>  [ $post->ID ],
		'cat'               =>  !empty($categories) ? $categories[0]->term_id : '', 
	);
	$related_query = new WP_Query ( $args );
	?>
	<?php if ( $related_query-> have_posts() ){ ?> 
		<div class="related-post">
			<h3 class="related-post-title"><?php esc_html_e('Related Articles', 'influencer-internship'); ?></h3>
			<div class="related-post-wrap clearfix">
				<?php while ( $related_query-> have_posts() ){
					$related_query->the_post();
					get_template_part( 'inc/parts/relatedposts' );
				} ?>
			</div>
		</div>
	<?php wp_reset_postdata(); }
}

if( ! function_exists( 'influencer_internship_latest_post' ) ) :
	function influencer_internship_latest_post(){ 
		$news_title		= get_theme_mod( 'news_title_setting', __( 'Latest News', 'influencer-internship' ) );
		$news_subtitle	= get_theme_mod( 'news_subtitle_setting',  __( 'TIPS FOR GETTING THINGS DONE', 'influencer-internship' ) );
		$latest_query 	= new WP_Query(array(
			'posts_per_page'	=> 3,
		)); ?>
			<section class="news-section">
				<?php if ( $latest_query->have_posts() ){ ?> 
					<div class="cm-wrapper">
						<p class="section-subtitle"><?php echo esc_html( $news_subtitle ); ?></p>
						<h1 class="section-title"><?php echo esc_html( $news_title ); ?></h1>
							<div class="news-block-wrap clearfix">
								<?php while ( $latest_query->have_posts() ){
									$latest_query->the_post();
									get_template_part( 'inc/parts/latestposts' );
									} ?>
							</div>
					</div>
					<?php }  wp_reset_postdata(); ?>
				<a class="bttn" href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>"><?php _e( 'More From The Blog','influencer-internship' ); ?></a>
			</section>
	<?php }
endif;

function influencer_internship_comments( $comment, $args, $depth ){
	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
	?>
		<<?php echo $tag ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
		<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body" itemscope itemtype="https://schema.org/UserComments">
		<?php endif; ?>
		
		<footer class="comment-meta">

			<div class="comment-author vcard">
				<?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
				<?php printf( __( '<b class="fn" itemprop="creator" itemscope itemtype="https://schema.org/Person">%s</b>', 'influencer-internship' ), get_comment_author_link() ); ?>
			</div>
		
			<div class="comment-metadata"><a href="<?php echo esc_url( htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ); ?>"><time itemprop="commentTime" datetime="<?php comment_date(); ?>">
				<?php
					/* translators: 1: date, 2: time */
					echo get_comment_date(); _e( ' at ', 'influencer-internship' );
					echo get_comment_time( 'g:i a' ); ?>
					</time>
					</a>
			</div>
			<?php if ( $comment->comment_approved == '0' ) : ?>
				<p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'influencer-internship' ); ?></p>
				<br />
			<?php endif; ?>
			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'], ) ) ); ?>
			</div>
		</footer>
		
		<div class="comment-content" itemprop="commentText"><?php comment_text(); ?></div>

		<?php if ( 'div' != $args['style'] ) : ?>
	</div>
		<?php endif; ?>
<?php }