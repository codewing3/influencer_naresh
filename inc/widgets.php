<?php
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function influencer_internship_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'influencer-internship' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'influencer-internship' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
	);

	register_sidebars( 5,
		array(
			'name'          => esc_html__( 'Footer %d' ),
			'id'            => 'footer',
			'description'   => esc_html__( 'Add widgets here.', 'influencer-internship' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
	);
}
add_action( 'widgets_init', 'influencer_internship_widgets_init' );