<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package influencer-internship
 */

get_header();
$blog_layout_option = get_theme_mod( 'blog_layout_option' );
?>
	

<?php
	if ( have_posts() ) :
		if ( is_home() && ! is_front_page() ) :
			?>
			<header>
				<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
			</header>
		<?php endif; ?>
		
		<div id="content" class="site-content">
			<?php influencer_internship_before_header_index(); ?>
			<div class="cm-wrapper">
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<div class="article-group <?php echo $blog_layout_option;?>">
							<?php 
							while ( have_posts() ) :
								the_post();
								get_template_part( 'template-parts/content', get_post_type() );
							endwhile;
							influencer_internship_pagination();
							?>
						</div>
					</main>
				</div>
				<?php
				else :
					get_template_part( 'template-parts/content', 'none' );
				endif;
				?>
				<?php get_sidebar();?>
			</div>
		</div>

<?php
get_footer();
