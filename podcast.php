<?php get_header(); ?>
    <div id="content" class="site-content">
        <div class="page-header" style="background: url(images/header-bg.jpg) no-repeat;">
            <div class="cm-wrapper">
                <h1 class="page-title">Podcast</h1>
                <div class="breadcrumb">
                    <a class="breadcrumb-item" href="#">Home</a>
                    <span class="seperator"><i class="fa fa-caret-right"></i></span>
                    <span class="breadcrumb-item current">Podcast</span>
                </div>
                <a href="#primary" class="scroll-down"></a>
            </div>
        </div>
        <div class="cm-wrapper">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="article-group list-layout">
                        <article>
                            <figure><a href="#" class="entry-image"><img src="images/audio-author1.jpg" alt="audion author"></a></figure>
                            <div class="entry-content">
                                <header class="entry-header">
                                    <div class="entry-meta">
                                        <div class="tag-group">
                                            <span>Episode 6</span>
                                        </div>
                                        <div class="article-author">
                                            <a href="#" class="url">Will Marvin</a>
                                        </div>
                                        <span class="article-author-pos">VP of Content,BuzzFeed Media</span>
                                    </div>
                                    <h2 class="entry-title">How Apple Surprises Customers and Transforms the Experience</h2>
                                </header>
                                <p>Sifting through teaspoons of clay and sand scraped from the floors of caves, German researchers have managed to isolate ancient human DNA — without turning up a single bone. Their new technique, described in a study published on Thursday in the journal Science, promises to open</p>
                                <img src="images/audio-player.jpg" alt="audio player">
                            </div>
                        </article>

                        <article>
                            <figure><a href="#" class="entry-image"><img src="images/audio-author2.jpg" alt="audion author"></a></figure>
                            <div class="entry-content">
                                <header class="entry-header">
                                    <div class="entry-meta">
                                        <div class="tag-group">
                                            <span>Episode 5</span>
                                        </div>
                                        <div class="article-author">
                                            <a href="#" class="url">Dixie Armstrong</a>
                                        </div>
                                        <span class="article-author-pos">VP of Content,BuzzFeed Media</span>
                                    </div>
                                    <h2 class="entry-title">7 Tips for Increasing Engagement with Your Blog</h2>
                                </header>
                                <p>Sifting through teaspoons of clay and sand scraped from the floors of caves, German researchers have managed to isolate ancient human DNA — without turning up a single bone. Their new technique, described in a study published on Thursday in the journal Science, promises to open</p>
                                <img src="images/audio-player.jpg" alt="audio player">
                            </div>
                        </article>

                        <article>
                            <figure><a href="#" class="entry-image"><img src="images/audio-author3.jpg" alt="audion author"></a></figure>
                            <div class="entry-content">
                                <header class="entry-header">
                                    <div class="entry-meta">
                                        <div class="tag-group">
                                            <span>Episode 4</span>
                                        </div>
                                        <div class="article-author">
                                            <a href="#" class="url">Eula Wolff</a>
                                        </div>
                                        <span class="article-author-pos">VP of Content,BuzzFeed Media</span>
                                    </div>
                                    <h2 class="entry-title">Marketing Associations to Blame Publishers for Allowing Bad Ads</h2>
                                </header>
                                <p>Sifting through teaspoons of clay and sand scraped from the floors of caves, German researchers have managed to isolate ancient human DNA — without turning up a single bone. Their new technique, described in a study published on Thursday in the journal Science, promises to open</p>
                                <img src="images/audio-player.jpg" alt="audio player">
                            </div>
                        </article>

                        <article>
                            <figure><a href="#" class="entry-image"><img src="images/audio-author4.jpg" alt="audion author"></a></figure>
                            <div class="entry-content">
                                <header class="entry-header">
                                    <div class="entry-meta">
                                        <div class="tag-group">
                                            <span>Episode 3</span>
                                        </div>
                                        <div class="article-author">
                                            <a href="#" class="url">Genoveva Leannon</a>
                                        </div>
                                        <span class="article-author-pos">VP of Content,BuzzFeed Media</span>
                                    </div>
                                    <h2 class="entry-title">Is Google's Subscription Gift to Publishers a Trojan Horse?</h2>
                                </header>
                                <p>Sifting through teaspoons of clay and sand scraped from the floors of caves, German researchers have managed to isolate ancient human DNA — without turning up a single bone. Their new technique, described in a study published on Thursday in the journal Science, promises to open</p>
                                <img src="images/audio-player.jpg" alt="audio player">
                            </div>
                        </article>

                        <article>
                            <figure><a href="#" class="entry-image"><img src="images/audio-author5.jpg" alt="audion author"></a></figure>
                            <div class="entry-content">
                                <header class="entry-header">
                                    <div class="entry-meta">
                                        <div class="tag-group">
                                            <span>Episode 2</span>
                                        </div>
                                        <div class="article-author">
                                            <a href="#" class="url">Michael Baker</a>
                                        </div>
                                        <span class="article-author-pos">VP of Content,BuzzFeed Media</span>
                                    </div>
                                    <h2 class="entry-title">Mayo Clinic Invests in Voice Search with Amazon</h2>
                                </header>
                                <p>Sifting through teaspoons of clay and sand scraped from the floors of caves, German researchers have managed to isolate ancient human DNA — without turning up a single bone. Their new technique, described in a study published on Thursday in the journal Science, promises to open</p>
                                <img src="images/audio-player.jpg" alt="audio player">
                            </div>
                        </article>

                    </div>

                    <nav class="navigation pagination">
                        <div class="nav-links">
                            <a class="page-numbers prev" href="#">
                                <span class="prev-arrow"></span> <span class="pagination-txt">Previous</span>
                            </a>
                            <span class="page-numbers current">
                                1
                            </span>
                            <a class="page-numbers" href="#">
                                2
                            </a>
                            <a class="page-numbers" href="#">
                                3
                            </a>
                            <span class="page-numbers dots">...</span>
                            <a class="page-numbers" href="#">
                                8
                            </a>
                            <a class="page-numbers" href="#">
                                12
                            </a>
                            <a class="page-numbers next" href="#">
                                <span class="pagination-txt">Next</span> <span class="next-arrow"></span>
                            </a>
                        </div>
                    </nav> <!-- .navigation.pagination -->

                </main>
            </div>
        </div>
        <div class="newsletter-wrap">
            <img src="images/form.jpg" alt="form">
        </div>
    </div>
<?php get_footer(); ?>