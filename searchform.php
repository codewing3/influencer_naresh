<form class="search-form">
    <label>
        <input type="search" name="search" class="search-field" placeholder="Search">
    </label>
    <label for="search-button">
        <input type="submit" id="search-button" name="submit" value="" class="search-submit">
    </label>
</form>