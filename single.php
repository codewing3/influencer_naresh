<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package influencer-internship
 */

get_header();

?>
	<div id="content" class="site-content"> 
		<?php influencer_internship_single_header(); ?>
		<div class="cm-wrapper">
			<div id="primary" class="content-area">
			<?php while(have_posts() ){the_post();
				get_template_part( 'template-parts/content', 'single' );
				}
				influencer_internship_post_navigation_part(); 
				$metakey = get_post_meta( get_the_ID(),'_width_meta',true );
				if ( $metakey === 'rightsidebar'){ 
					influencer_internship_related_post_categories();
					influencer_internship_comments_part();
					?> </div> <?php
					get_sidebar();
				} else {
					influencer_internship_comments_part();
					influencer_internship_related_post_categories();
					?> </div> <?php
				} ?>
			</div>
		</div>
	</div> 
<?php
get_footer();
