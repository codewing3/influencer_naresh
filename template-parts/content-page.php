<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package influencer-internship
 */
global $post;
$page_image	= get_theme_mod( 'page_background_setting', get_template_directory_uri() . '/images/header-bg.jpg' );
$page_title	= get_theme_mod( 'page_title_setting', __( 'Landing Page', 'influencer-internship') );
$post_image = get_the_post_thumbnail_url( $post->ID );
?> 
	<div id="page" class="site">
		<div id="content" class="site-content">
		<?php if ( has_post_thumbnail() ){ ?>
			<div class="page-header" style="background: url(<?php echo $post_image; ?>) no-repeat;">
		<?php } else { ?>
			<div class="page-header" style="background: url(<?php echo $page_image; ?>) no-repeat;">
		<?php } ?>
				<div class="cm-wrapper">
					<h1 class="page-title"><?php echo $page_title; ?></h1>
					<a href="#primary" class="scroll-down"></a>
				</div>
			</div>
			<div class="cm-wrapper">
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<div class="article-group">
							<?php the_content(); ?>
						</div>
					</main>
				</div>
			</div>
		</div>
	</div>
</body>
<?php get_footer(); ?>