<div class="article-group">
    <article itemscope itemtype="http://schema.org/Article">
        <div class="side-social-share">
            <img src="<?php echo esc_url(get_template_directory_uri() . '/images/side-social-share.jpg' ); ?>" alt="side social">
        </div>
        <?php esc_html_e( the_content() ); ?>
        <div class="tag-share-wrap">
            <?php get_template_part('inc/parts/tags'); ?>
            <div class="bottom-share-block">
                <img src="<?php echo esc_url(get_template_directory_uri() . '/images/bottom-share.jpg' ); ?>" alt="bottom share">
            </div>
        </div>
    </article>
</div>

<?php influencer_internship_author_newsletter_wrap(); ?>

