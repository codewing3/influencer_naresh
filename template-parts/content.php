<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">
<?php 
$blog_layout_option = get_theme_mod( 'blog_layout_option' );

	if ( $blog_layout_option == 'default-layout' ) {
		influencer_internship_default_style_content();
	} else {
		influencer_internship_grid_content();
	} ?>
	
</article><!-- #post-<?php the_ID(); ?> -->